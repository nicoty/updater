#!/bin/sh

# dependencies: `lib.sh`, `firefox`

main () { (
# -e = exit if any statement returns a non-true return value. -u = exit if an attempt is made to use an uninitialised variable.
  set -eu &&

# include libs
  sh_lib="${HOME}/.local/lib/lib.sh"
  sh_lib_update="${HOME}/.local/lib/lib_update.sh"

# shellcheck source=../lib/lib.sh
  . "${sh_lib}" &&
  . "${sh_lib_update}" &&

# set trap
  trap 'notify_exit_early' HUP INT QUIT ABRT ALRM TERM &&

# warn
  warning_privilege &&

# set variables
  file_self="$(basename "$(readlink -f "${0}")")"
  version_self="0.1.0"
# will_exit used to allow other options to be parsed too
  will_exit=1
  name_profile="ghacks"
  directory_profile="${HOME}/.mozilla/firefox/${name_profile}"
  directory_ghacksuserjs="${HOME}/development/github/ghacksuserjs"
  directory_ghacks_user_js="${directory_ghacksuserjs}/ghacks-user.js"
  repository_ghacks_user_js="https://github.com/ghacksuserjs/ghacks-user.js.git"
  branch_ghacks_user_js="master"
  will_update_ghacks_user_js=1
  directory_12bytes_org="${HOME}/development/codeberg/12bytes.org"
  directory_firefox_user_js_supplement="${directory_12bytes_org}/Firefox-user.js-supplement"
  repository_firefox_user_js_supplement="https://codeberg.org/12bytes.org/Firefox-user.js-supplement.git"
  branch_firefox_user_js_supplement="master"
  will_update_firefox_user_js_supplement=1
  if test -f "$(readlink --canonicalize "${HOME}/.config/update/user-overrides.js.appendix")" ; then
    path_user_overrides_js_appendix="${HOME}/.config/update/user-overrides.js.appendix"
  else
    path_user_overrides_js_appendix="${HOME}/.local/share/update/user-overrides.js.appendix"
  fi
  if test -f "$(readlink --canonicalize "${HOME}/.config/update/${file_self}.conf")" ; then
    conf_self="${HOME}/.config/update/${file_self}.conf"
  else
    conf_self="${HOME}/.local/share/update/${file_self}.conf"
  fi

# apply configuration
  if test -f "$(readlink --canonicalize "${conf_self}")" ; then
    number_line=0
    while IFS='
' read -r configuration ; do
#     remove the first space and everything after it, then use the remainder as the variable name. remove the first space and everything before it, then use the remainder as the value
      variable="${configuration%% *}"
      value="${configuration#* }"
      number_line="$((number_line + 1))"
      case "${configuration}" in
        "sh_lib "?* | "name_profile "?* | "directory_profile "?* | "directory_ghacksuserjs "?* | "directory_ghacks_user_js "?* | "repository_ghacks_user_js "?* | "branch_ghacks_user_js "?* | "directory_12bytes_org "?* | "directory_firefox_user_js_supplement "?* | "repository_firefox_user_js_supplement "?* | "branch_firefox_user_js_supplement "?* | "path_user_overrides_js_appendix "?* )
          eval "${variable}"="${value}"
        ;;
        * )
          error_configuration_invalid >&2
        ;;
      esac
    done < "${conf_self}"
  fi &&

# define internal functions
  print_help () {
    printf 'Download/update then install
`ghacksuserjs/ghacks-user.js`.

USAGE:
  %s [FLAGS]

FLAGS:
  -x, --debug
    Print commands and their arguments as they are
    executed. 

  -h, --help
    Print this message then exit.

  -V, --version
    Print version information then exit.
' "${file_self}" >&2
  }

# create profile if required
  create_profile_if_required () { (
    name_profile="${1}"
    directory_profile="${2}"

    if ! test -d "$(readlink --canonicalize "${directory_profile}")" ; then
      firefox -CreateProfile "${name_profile} ${directory_profile}" -no-remote
    fi
  ) }

# argument parsing loop from https://stackoverflow.com/a/28466267
  while getopts hVx-: argument; do
    case "${argument}" in
      h )
        print_help >&2 &&
        will_exit=0
      ;;
      V )
        print_version "${file_self}" "${version_self}" >&2 &&
        will_exit=0
      ;;
      x )
        set -x
      ;;
      - )
        case "${OPTARG}" in
          help )
            print_help >&2 &&
            will_exit=0
          ;;
          version )
            print_version "${file_self}" "${version_self}" >&2 &&
            will_exit=0
          ;;
          debug )
            set -x
          ;;
          help* | version* | debug* )
            error_arguments_contained >&2
          ;;
#         "--" terminates argument processing
          '' )
            break
          ;;
          * )
            error_option_invalid >&2
          ;;
        esac
      ;;
#     getopts already reported the illegal option
      \? )
        exit 2
      ;;
    esac
  done &&
# remove parsed options and arguments from "${@}" list
  shift $((OPTIND-1)) &&

  if test "${will_exit}" -eq 0 ; then
    exit 0
  fi &&

# main
  create_profile_if_required "${name_profile}" "${directory_profile}" &&
  if git_repository_update "${directory_ghacksuserjs}" "${directory_ghacks_user_js}" "${repository_ghacks_user_js}" "${branch_ghacks_user_js}" ; then
    will_update_ghacks_user_js=0
    install --mode=755 "${directory_ghacks_user_js}/updater.sh" "${directory_profile}/" &&
    install --mode=755 "${directory_ghacks_user_js}/prefsCleaner.sh" "${directory_profile}/"
  fi &&
  if git_repository_update "${directory_12bytes_org}" "${directory_firefox_user_js_supplement}" "${repository_firefox_user_js_supplement}" "${branch_firefox_user_js_supplement}" ; then
    will_update_firefox_user_js_supplement=0
    install "${directory_firefox_user_js_supplement}/user-overrides.js" "${directory_profile}/"
  fi &&
  if test "${will_update_ghacks_user_js}" -eq 0 || test "${will_update_firefox_user_js_supplement}" -eq 0 ; then
    cd "${directory_profile}/" &&
#   append appendix if it exists
    if test -f "${path_user_overrides_js_appendix}" && test "${will_update_firefox_user_js_supplement}" -eq 0 ; then
      cat "${path_user_overrides_js_appendix}" >> "./user-overrides.js"
    fi &&
#   remove user.js to prevent bloat
    if test -f "$(readlink -f "./user.js")" ; then
      rm "./user.js"
    fi &&
    bash ./updater.sh &&
    bash ./prefsCleaner.sh
  fi
) }

main "${@}"
