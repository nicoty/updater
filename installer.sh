#!/bin/sh
# todo: installation for root
# dependencies: `help2man`

# shellcheck source=../../../.profile
main () {
# -e = exit if any statement returns a non-true return value. -u = exit if an attempt is made to use an uninitialised variable.
  set -eu &&

# include libs
# directory that contains this script
  directory_self="$(dirname "$(readlink -f "${0}")")"
# todo: verify that other scripts are present, else exit
  sh_lib="${HOME}/.local/lib/lib.sh"

# shellcheck source=lib/lib.sh
  . "${sh_lib}" &&

# make the remove list for trap
  list_rm="$(mktemp)"

# set trap
  trap 'notify_exit_early "${list_rm}"' HUP INT QUIT ABRT ALRM TERM &&

# warn
  warning_privilege &&

# set variables
  file_self="$(basename "$(readlink -f "${0}")")"
  version_self="0.1.0"
  will_exit=1
  boolean_uninstall=1
  dependencies="curl sudo git nft xi rustup rustc cargo cargo-install-update make help2man"
  directory_bin="${HOME}/.local/bin"
  directory_conf="${HOME}/.local/share/update"
  directory_applications="${HOME}/.local/share/applications"
  directory_man="${HOME}/.local/share/man"
  directory_man1="${directory_man}/man1"

# define internal functions
  print_help () {
    printf 'Install(default)/uninstall `update`.

USAGE:
  sh %s [FLAGS]

FLAGS:
  -x, --debug
    Print commands and their arguments
    as they are executed.

  -h, --help
    Print this message then exit.

  -r, -u, --remove, --uninstall
    Uninstall `svc`, `update`
    and `update-user-js`.

  -V, --version
    Print version information then
    exit.
' "${file_self}" >&2
  }

export_manpath () {
  printf '\n# this line and the one below were added by installer.sh\nexport MANPATH="%s:"\n' "${directory_man}" >> "${HOME}/.profile"
}

# argument parsing loop from https://stackoverflow.com/a/28466267
  while getopts hruVx-: argument; do
    case "${argument}" in
      h )
        print_help >&2 &&
        will_exit=0
      ;;
      r | u )
        boolean_uninstall=0
      ;;
      V )
        print_version "${file_self}" "${version_self}" >&2 &&
        will_exit=0
      ;;
      x )
        set -x
      ;;
      - )
        case "${OPTARG}" in
          help )
            print_help >&2 &&
            will_exit=0
          ;;
          remove | uninstall )
            boolean_uninstall=0
          ;;
          version )
            print_version "${file_self}" "${version_self}" >&2 &&
            will_exit=0
          ;;
          debug )
            set -x
          ;;
          help* | remove* | uninstall* | version* | debug* )
            error_arguments_contained >&2 &&
            exit 2
          ;;
#         "--" terminates argument processing
          '' )
            break
          ;;
          * )
            error_option_invalid >&2 &&
            exit 2
          ;;
        esac
      ;;
#     getopts already reported the illegal option
      \? )
        exit 2
      ;;
    esac
  done &&
# remove parsed options and arguments from "${@}" list
  shift $((OPTIND-1)) &&

  if test "${will_exit}" -eq 0 ; then
    exit 0
  fi &&

# main
  if test "${boolean_uninstall}" -ne 0 ; then
#   install
#   iterate through dependencies array and check if any are not installed
    printf '%s\n' "${dependencies}" | tr ' ' '\n' | while read -r item ; do
      if test -z "$(command -v "${item}")" ; then
        error "Dependency \"${item}\" is not installed.\n" "1" >&2
      fi
    done &&

#   make the necessary directories if required
    mkdir --parents "${directory_bin}" "${directory_conf}" "${directory_applications}" "${directory_man1}" &&

#   todo: make backup, install to temp file. only install if printf was succesful, other otherwise remove temp file
#   add "${PATH}" and "${MANPATH}" entries to .profile
    case "${PATH}" in
      *"${directory_bin}"* )
#       "${directory_bin}" is already in "${PATH}", do nothing
      ;;
      * )
#       "${directory_bin}" is not yet in "${PATH}", add it by appending the following to .profile
        printf '\n# this line and the one below were added by installer.sh\nexport PATH="%s:$PATH"\n' "${directory_bin}" >> "${HOME}/.profile"
      ;;
    esac &&

    if test "${MANPATH:+set}" = "set" ; then
#     $MANPATH has been set and is not null-valued
      case "${MANPATH}" in
        *"${directory_man}"* )
#         "${directory_man}" is already in "${MANPATH}", do nothing
        ;;
        * )
#         "${directory_man}" is not yet in "${MANPATH}", add it by appending an export to .profile
          export_manpath
        ;;
      esac
    else
#     $MANPATH has not been set, so "${directory_man}" is not yet in "${MANPATH}", add it by appending an export to .profile
      export_manpath
    fi &&

#   apply .profile to current shell
    . "${HOME}/.profile" &&

# BROKEN: doesn't install configuration files properly
# TODO: make this so that file containing a list of contents is used and script just installs stuff according to the list in the file. maybe script should make a file containing list of what was installed and refer to that file when uninstalling to determine what to remove? also, script should ask if configuration files should be overwritten or uninstalled.
#   install executable files, their manuals, and configuration files
    for path_file in "${directory_self}/bin/"?* ; do
      if test -f "$(readlink --canonicalize "${path_file}")" ; then
        name_file="$(basename "${path_file}")"
        install --compare "${path_file}" "${directory_bin}/${name_file}" &&
        printf '%s/%s\n' "${directory_bin}" "${name_file}" >> "${list_rm}" &&
        help2man --no-discard-stderr "${name_file}" > "${directory_man1}/${name_file}.1" &&
        printf '%s/%s.1\n' "${directory_man1}" "${name_file}" >> "${list_rm}" &&
        if test -f "$(readlink --canonicalize "${directory_self}/bin/share/${name_file}")" ; then
#         configuration file is present, install it
          install --compare "${directory_self}/bin/share/${name_file}" "${directory_conf}/${name_file}" &&
          printf '%s/%s\n' "${directory_conf}" "${name_file}" >> "${list_rm}"
        fi
      fi
    done &&
#   install desktop shortcuts if present
    if test -d "$(readlink --canonicalize "${directory_self}/applications")" ; then
      for path_file in "${directory_self}/applications/"?*; do
        name_file="$(basename "${path_file}")"
        install --compare "${path_file}" "${directory_applications}/${name_file}" &&
        printf '%s/%s\n' "${directory_applications}" "${name_file}" >> "${list_rm}"
      done
    fi

  else
#   uninstall
#   remove executable files and their manuals
    for path_file in "${directory_self}/bin/"?* ; do
      if test -f "$(readlink --canonicalize "${path_file}")" ; then
        name_file="$(basename "${path_file}")"
        rm --recursive "${directory_bin:?}/${name_file}" "${directory_man1:?}/${name_file}.1" &&
        if test -f "$(readlink --canonicalize "${directory_conf}/${name_file}")" ; then
#         configuration files were present, remove them
          rm --recursive "${directory_conf:?}/${name_file}"
        fi
      fi
    done &&
#   remove shortcuts if present
    if test -d "$(readlink --canonicalize "${directory_self}/applications")" ; then
      for path_file in "${directory_self}/applications/"?* ; do
        rm --recursive "${directory_applications:?}/$(basename "${path_file}")"
      done
    fi
  fi &&

# remove remove list for trap
  rm "${list_rm}"
}

main "${@}"
